<?php
/**
 * Template Name: Section Page Template
 *
 *
 * @package WordPress
 */

get_header(); ?>


		
<?php
if( have_rows('sections') ):
$count=1;
    while ( have_rows('sections') ) : the_row();
?>
<div id="sections<?php echo $count ?>" class="sections entry-content <?php the_sub_field('custom_classes') ?>" style="background-color:<?php the_sub_field('background_color') ?>;background-image:url(<?php the_sub_field('background_image') ?>);color:<?php the_sub_field('color') ?>;padding:<?php the_sub_field('padding') ?> 0px">
	<?php the_sub_field('content') ?>
</div>
	
<?php
$count++;
    endwhile;
endif;
?>
<?php if ( is_page(321) ) { ?>
<div class="flex bg-gray-300 flex-wrap" id="sections3">
<div class="w-full md:w-1/2 py-12 px-24 text-center">
 <?php the_field('contact_content') ?>
</div>
<div class="w-full md:w-1/2">
<?php the_field('location') ?>
</div>
</div>
<?php } ?>

<?php get_footer(); 