<?php
/**
 * Template Name: Full-width Page Template, No Sidebar
 *
 *
 * @package WordPress
 */

get_header(); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div id="primary" class="site-content">
				<div id="content" role="main">
		
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content', 'page' ); ?>
						<?php comments_template( '', true ); ?>
					<?php endwhile; // end of the loop. ?>
		
				</div><!-- #content -->
			</div><!-- #primary -->
		</div>
	</div>
</div>
<?php get_footer(); 