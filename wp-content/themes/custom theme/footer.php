<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 */
?>
	</div><!-- #main .wrapper -->
</div><!-- #page -->

<div class="bg-gray-800">
	<div class="flex flex-wrap mx-0 md:mx-12 md:mx-24">
		<div class="w-full md:w-1/3 bg-red-600">
			<div class="p-8 text-white">
				<?php the_field('footer_about_content', 'options'); ?>
			</div>
		</div>
		<div class="w-full md:w-2/3 sm:flex text-white list-none py-2 md:py-8 px-4 md:px-16">
			<div class="w-1/3 md:text-left sm:text-center">
				<?php
					if(is_active_sidebar('footer-1')){
					dynamic_sidebar('footer-1');
					}
				?>
			</div>
			<div class="w-1/3 md:text-left sm:text-center">
				<?php
					if(is_active_sidebar('footer-2')){
					dynamic_sidebar('footer-2');
					}
				?>
			</div>
			<div class="w-1/3 md:text-left sm:text-center">
				<?php
					if(is_active_sidebar('footer-3')){
					dynamic_sidebar('footer-3');
					}
				?>
			</div>
		</div>
	</div>
</div>



<div class="flex bg-black md:px-24 py-8 md:py-8 px-8">
	<div class="text-xs md:text-lg text-white w-full md:w-1/2">
		<?php the_field('bottom_left', 'options'); ?>
	</div>
	<div class="text-xs md:text-lg  text-white text-right w-full md:w-1/2">
		<?php the_field('bottom_right', 'options'); ?>
	</div>
</div>


<?php wp_footer(); ?>

</body>
</html>