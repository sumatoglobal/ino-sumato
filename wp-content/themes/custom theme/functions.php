<?php
/**
 * Custom Theme functions and definitions.
 */
// require_once(get_stylesheet_directory().'/inc/class-wp-bootstrap-navwalker.php');
if ( ! isset( $content_width ) )
	$content_width = 625;
function custom_setup() {
	load_theme_textdomain( 'custom', get_template_directory() . '/languages' );
	add_editor_style();
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );
	register_nav_menu( 'primary', __( 'Primary Menu', 'custom' ) );
	add_theme_support( 'custom-background', array(
		'default-color' => 'e6e6e6',
	) );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 624, 9999 ); // Unlimited height, soft crop

}
add_action( 'after_setup_theme', 'custom_setup' );


function custom_get_font_url() {
	$font_url = '';

	/* translators: If there are characters in your language that are not supported
	 by Open Sans, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Open Sans font: on or off', 'custom' ) ) {
		$subsets = 'latin,latin-ext';

		/* translators: To add an additional Open Sans character subset specific to your language, translate
		 this to 'greek', 'cyrillic' or 'vietnamese'. Do not translate into your own language. */
		$subset = _x( 'no-subset', 'Open Sans font: add new subset (greek, cyrillic, vietnamese)', 'custom' );

		if ( 'cyrillic' == $subset )
			$subsets .= ',cyrillic,cyrillic-ext';
		elseif ( 'greek' == $subset )
			$subsets .= ',greek,greek-ext';
		elseif ( 'vietnamese' == $subset )
			$subsets .= ',vietnamese';

		$protocol = is_ssl() ? 'https' : 'http';
		$query_args = array(
			'family' => 'Open+Sans:400italic,700italic,400,700',
			'subset' => $subsets,
		);
		$font_url = add_query_arg( $query_args, "$protocol://fonts.googleapis.com/css" );
	}

	return $font_url;
}

function custom_scripts_styles() {
	global $wp_styles;
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	wp_enqueue_script( 'custom-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '1.0', true );

	$font_url = custom_get_font_url();
	if ( ! empty( $font_url ) )
		wp_enqueue_style( 'custom-fonts', esc_url_raw( $font_url ), array(), null );
	wp_enqueue_style( 'custom-style', get_stylesheet_uri() );

	wp_enqueue_style( 'custom-ie', get_template_directory_uri() . '/css/ie.css', array( 'custom-style' ), '20121010' );
	$wp_styles->add_data( 'custom-ie', 'conditional', 'lt IE 9' );
}
add_action( 'wp_enqueue_scripts', 'custom_scripts_styles' );

function custom_mce_css( $mce_css ) {
	$font_url = custom_get_font_url();

	if ( empty( $font_url ) )
		return $mce_css;

	if ( ! empty( $mce_css ) )
		$mce_css .= ',';

	$mce_css .= esc_url_raw( str_replace( ',', '%2C', $font_url ) );

	return $mce_css;
}
add_filter( 'mce_css', 'custom_mce_css' );

function custom_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'custom' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'custom_wp_title', 10, 2 );

function custom_page_menu_args( $args ) {
	if ( ! isset( $args['show_home'] ) )
		$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'custom_page_menu_args' );

function custom_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Main Sidebar', 'custom' ),
		'id' => 'sidebar-1',
		'description' => __( 'Appears on posts and pages except the optional Front Page template, which has its own widgets', 'custom' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => 'Footer Widgets',
		'id' => 'footer-1',
		'description' => 'Appears in the footer area',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
	register_sidebar( array(
		'name' => 'Footer Widgets',
		'id' => 'footer-2',
		'description' => 'Appears in the footer area',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
	register_sidebar( array(
		'name' => 'Footer Widgets',
		'id' => 'footer-3',
		'description' => 'Appears in the footer area',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
	register_sidebar( array(
		'name' => 'Footer Widgets',
		'id' => 'footer-4',
		'description' => 'Appears in the footer area',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
}
add_action( 'widgets_init', 'custom_widgets_init' );

if ( ! function_exists( 'custom_content_nav' ) ) :

function custom_content_nav( $html_id ) {
	global $wp_query;

	$html_id = esc_attr( $html_id );

	if ( $wp_query->max_num_pages > 1 ) : ?>
		<nav id="<?php echo $html_id; ?>" class="navigation" role="navigation">
			<h3 class="assistive-text"><?php _e( 'Post navigation', 'custom' ); ?></h3>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'custom' ) ); ?></div>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'custom' ) ); ?></div>
		</nav><!-- #<?php echo $html_id; ?> .navigation -->
	<?php endif;
}
endif;

if ( ! function_exists( 'custom_comment' ) ) :

function custom_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php _e( 'Pingback:', 'custom' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'custom' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<header class="comment-meta comment-author vcard">
				<?php
					echo get_avatar( $comment, 44 );
					printf( '<cite><b class="fn">%1$s</b> %2$s</cite>',
						get_comment_author_link(),
						// If current post author is also comment author, make it known visually.
						( $comment->user_id === $post->post_author ) ? '<span>' . __( 'Post author', 'custom' ) . '</span>' : ''
					);
					printf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
						esc_url( get_comment_link( $comment->comment_ID ) ),
						get_comment_time( 'c' ),
						/* translators: 1: date, 2: time */
						sprintf( __( '%1$s at %2$s', 'custom' ), get_comment_date(), get_comment_time() )
					);
				?>
			</header><!-- .comment-meta -->

			<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'custom' ); ?></p>
			<?php endif; ?>

			<section class="comment-content comment">
				<?php comment_text(); ?>
				<?php edit_comment_link( __( 'Edit', 'custom' ), '<p class="edit-link">', '</p>' ); ?>
			</section><!-- .comment-content -->

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'custom' ), 'after' => ' <span>&darr;</span>', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</article><!-- #comment-## -->
	<?php
		break;
	endswitch; // end comment_type check
}
endif;

if ( ! function_exists( 'custom_entry_meta' ) ) :

function custom_entry_meta() {
	// Translators: used between list items, there is a space after the comma.
	$categories_list = get_the_category_list( __( ', ', 'custom' ) );

	// Translators: used between list items, there is a space after the comma.
	$tag_list = get_the_tag_list( '', __( ', ', 'custom' ) );

	$date = sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a>',
		esc_url( get_permalink() ),
		esc_attr( get_the_time() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() )
	);

	$author = sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		esc_attr( sprintf( __( 'View all posts by %s', 'custom' ), get_the_author() ) ),
		get_the_author()
	);

	// Translators: 1 is category, 2 is tag, 3 is the date and 4 is the author's name.
	if ( $tag_list ) {
		$utility_text = __( 'This entry was posted in %1$s and tagged %2$s on %3$s<span class="by-author"> by %4$s</span>.', 'custom' );
	} elseif ( $categories_list ) {
		$utility_text = __( 'This entry was posted in %1$s on %3$s<span class="by-author"> by %4$s</span>.', 'custom' );
	} else {
		$utility_text = __( 'This entry was posted on %3$s<span class="by-author"> by %4$s</span>.', 'custom' );
	}

	printf(
		$utility_text,
		$categories_list,
		$tag_list,
		$date,
		$author
	);
}
endif;

function custom_body_class( $classes ) {
	$background_color = get_background_color();
	$background_image = get_background_image();

	if ( ! is_active_sidebar( 'sidebar-1' ) || is_page_template( 'page-templates/full-width.php' ) )
		$classes[] = 'full-width';

	if ( is_page_template( 'page-templates/front-page.php' ) ) {
		$classes[] = 'template-front-page';
		if ( has_post_thumbnail() )
			$classes[] = 'has-post-thumbnail';
		if ( is_active_sidebar( 'sidebar-2' ) && is_active_sidebar( 'sidebar-3' ) )
			$classes[] = 'two-sidebars';
	}

	if ( empty( $background_image ) ) {
		if ( empty( $background_color ) )
			$classes[] = 'custom-background-empty';
		elseif ( in_array( $background_color, array( 'fff', 'ffffff' ) ) )
			$classes[] = 'custom-background-white';
	}
	if ( wp_style_is( 'custom-fonts', 'queue' ) )
		$classes[] = 'custom-font-enabled';

	if ( ! is_multi_author() )
		$classes[] = 'single-author';

	return $classes;
}
add_filter( 'body_class', 'custom_body_class' );

function custom_content_width() {
	if ( is_page_template( 'page-templates/full-width.php' ) || is_attachment() || ! is_active_sidebar( 'sidebar-1' ) ) {
		global $content_width;
		$content_width = 960;
	}
}
add_action( 'template_redirect', 'custom_content_width' );

function custom_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'custom_customize_register' );

function custom_customize_preview_js() {
	wp_enqueue_script( 'custom-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), '20130301', true );
}
//Excerpt
function new_excerpt_length($length) {
    return 40;
}
add_filter('excerpt_length', 'new_excerpt_length');
function new_excerpt_more() {
	global $post;
	return '<div class="readmore_button"><a href="'. get_permalink($post->ID) . '" class="readmore">See More</a></div>';
}
add_filter('excerpt_more', 'new_excerpt_more');

// include_once('inc/boostrap/boostrap.php' );

add_filter('acf/settings/path', 'my_acf_settings_path');
function my_acf_settings_path( $path ) {
    $path = get_stylesheet_directory() . '/inc/acf/';
    return $path;
}
add_filter('acf/settings/dir', 'my_acf_settings_dir');
function my_acf_settings_dir( $dir ) {
    $dir = get_stylesheet_directory_uri() . '/inc/acf/';
    return $dir;
}
add_filter('acf/settings/show_admin', '__return_false');
include_once( get_stylesheet_directory() . '/inc/acf/acf.php' );
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
	acf_add_options_sub_page('General');
	acf_add_options_sub_page('Header');
  acf_add_options_sub_page('Homepage');
	acf_add_options_sub_page('Footer');

}
// Add Shortcode
function custom_shortcode( $atts , $content = null ) {
	$atts = shortcode_atts(
		array(
			'image' => '',
			'link' => '',
			'opacity' => '',
			'textcolor' => '',
			'class' => '',
			'height' => ''
		),
		$atts
	);
	ob_start();
	?>
	<div class="cta <?php echo $atts['class'] ?>" style="min-height:<?php echo $atts['height'] ?>;color:<?php echo $atts['textcolor'] ?>">
		<div class="image" style="background-image:url(<?php echo $atts['image'] ?>);"></div>
		<div class="overlay" style="opacity: <?php echo $atts['opacity'] ?>"></div>
		<div class="cta-content">
			<?php echo do_shortcode($content); ?>
		</div>
	</div>
	<?php
	return force_balance_tags(ob_get_clean());
}
add_shortcode( 'cta', 'custom_shortcode' );


function custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Our Teams', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Our Team', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Our Team', 'text_domain' ),
		'name_admin_bar'        => __( 'Our Team', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Our Team', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'custom-fields' ),
		'taxonomies'            => array( 'destination' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'our_team', $args );
	
	$labels = array(
		'name'                  => _x( 'Governments', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Government', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Government', 'text_domain' ),
		'name_admin_bar'        => __( 'Government', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Government', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'custom-fields' ),
		'taxonomies'            => array( 'destination' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'government', $args );
	
	$labels = array(
		'name'                  => _x( 'Industries', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Industry', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Industry', 'text_domain' ),
		'name_admin_bar'        => __( 'Industry', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Industry', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'custom-fields' ),
		'taxonomies'            => array( 'destination' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'industry', $args );
	
	$labels = array(
		'name'                  => _x( 'Agricultures', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Agriculture', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Agriculture', 'text_domain' ),
		'name_admin_bar'        => __( 'Agriculture', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Agriculture', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'custom-fields' ),
		'taxonomies'            => array( 'destination' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'agriculture', $args );
	
	$labels = array(
		'name'                  => _x( 'Supply Chains', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Supply Chain', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Supply Chain', 'text_domain' ),
		'name_admin_bar'        => __( 'Supply Chain', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Supply Chain', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'custom-fields' ),
		'taxonomies'            => array( 'destination' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'supply_chain', $args );
	
	}
add_action( 'init', 'custom_post_type', 0 );

add_shortcode( 'teams', 'teams' );
function teams( $atts ) {
    ob_start();
    $query = new WP_Query( array(
        'post_type' => 'our_team',
        'posts_per_page' => -1,
    ) );
    if ( $query->have_posts() ) { ?>
        <div class="flex flex-wrap">
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				<div class="mb-4 md:mb-0 w-1/2 sm:w-1/4 md:w-1/5 p-2 md:p-8">
					<div class="view" style="background-image:url('<?php the_field('featured_image'); ?>'); background-size: cover;">
							
							<div class="overlay">
								<div class="content">
									<h4 class="pb-0 md:pb-2 border-b text-sm md:text-xl font-medium"><?php the_title(); ?></h4>
									<p class="text-xs md:text-base subpixel-antialiased mb-2 mt-2 md:mt-0 md:mb-6"><?php the_field('excerpt'); ?></p>
								</div>
							</div>
					</div>
					<div class="contents md:hidden">
									<h4 class="pb-0 md:pb-2 border-b text-sm md:text-xl font-medium"><?php the_title(); ?></h4>
									<p class="text-xs md:text-base subpixel-antialiased mb-2 mt-2 md:mt-0 md:mb-6"><?php the_field('excerpt'); ?></p>
					</div>
				</div>
			<?php endwhile;
            wp_reset_postdata(); ?>
		</div>
    <?php $myvariable = ob_get_clean();
    return $myvariable;
    }
}


add_shortcode( 'supply-chains', 'supply_chains' );
function supply_chains( $atts ) {
    ob_start();
    $query = new WP_Query( array(
        'post_type' => 'supply_chain',
        'posts_per_page' => -1,
		'order' => 'ASC' 
    ) );
    if ( $query->have_posts() ) { ?>
        <div class="flex flex-wrap items-center px-4 md:px-40">
				<?php while ( $query->have_posts() ) : $query->the_post(); $counter++;
				$post_id = get_the_ID(); 
				if($counter%2 != 0){
				?>
					<div class="w-full md:w-1/2 p-4">
						<h1 class="text-3xl font-semibold heading-red"><?php the_title(); ?></h1>
						<?php the_content();?>
						
					</div>
					<div class="w-full md:w-1/2 p-4 md:p-16 text-gray-700">
						<img src="<?php the_field('featured_image'); ?>" alt="" width="100%" class="inline-block">
					</div>
			<?php }else { ?>
					<div class="w-full md:w-1/2 p-4 md:p-16 text-gray-700">
						<img src="<?php the_field('featured_image'); ?>" alt="" width="100%" class="inline-block">
					</div>	
					<div class="w-full md:w-1/2 p-4">
						<h1 class="text-3xl font-semibold heading-red"><?php the_title(); ?></h1>
						<?php the_content();?>
						
					</div>
			
			<?php } endwhile;
            wp_reset_postdata(); ?>
		</div>
    <?php $myvariable = ob_get_clean();
    return $myvariable;
    }
}



add_shortcode( 'government-project', 'government_project' );
function government_project( $atts ) {
    ob_start();
    $query = new WP_Query( array(
        'post_type' => 'government',
        'posts_per_page' => -1,
		'order' => 'ASC' 
    ) );
    if ( $query->have_posts() ) { ?>
        <div class="flex flex-wrap items-center px-4 md:px-40">
				<?php while ( $query->have_posts() ) : $query->the_post(); $counter++;
				$post_id = get_the_ID(); 
				if($counter%2 != 0){
				?>
					<div class="w-full md:w-1/2 p-4">
						<h1 class="text-3xl font-semibold heading-red"><?php the_title(); ?></h1>
						<?php the_content();?>
						
					</div>
					<div class="w-full md:w-1/2 p-4 md:p-16 text-gray-700">
						<img src="<?php the_field('featured_image'); ?>" alt="" width="100%" class="inline-block">
					</div>
			<?php }else { ?>
					<div class="w-full md:w-1/2 p-4 md:p-16 text-gray-700">
						<img src="<?php the_field('featured_image'); ?>" alt="" width="100%" class="inline-block">
					</div>	
					<div class="w-full md:w-1/2 p-4">
						<h1 class="text-3xl font-semibold heading-red"><?php the_title(); ?></h1>
						<?php the_content();?>
						
					</div>
			
			<?php } endwhile;
            wp_reset_postdata(); ?>
		</div>
    <?php $myvariable = ob_get_clean();
    return $myvariable;
    }
}


add_shortcode( 'industry-project', 'industry_project' );
function industry_project( $atts ) {
    ob_start();
    $query = new WP_Query( array(
        'post_type' => 'industry',
        'posts_per_page' => -1,
		'order' => 'ASC' 
    ) );
    if ( $query->have_posts() ) { ?>
        <div class="flex flex-wrap items-center px-4 md:px-40">
				<?php while ( $query->have_posts() ) : $query->the_post(); $counter++;
				$post_id = get_the_ID(); 
				if($counter%2 != 0){
				?>
					<div class="w-full md:w-1/2 p-4">
						<h1 class="text-3xl font-semibold heading-red"><?php the_title(); ?></h1>
						<?php the_content();?>
						
					</div>
					<div class="w-full md:w-1/2 p-4 md:p-16 text-gray-700">
						<img src="<?php the_field('featured_image'); ?>" alt="" width="100%" class="inline-block">
					</div>
			<?php }else { ?>
					<div class="w-full md:w-1/2 p-4 md:p-16 text-gray-700">
						<img src="<?php the_field('featured_image'); ?>" alt="" width="100%" class="inline-block">
					</div>	
					<div class="w-full md:w-1/2 p-4">
						<h1 class="text-3xl font-semibold heading-red"><?php the_title(); ?></h1>
						<?php the_content();?>
						
					</div>
			
			<?php } endwhile;
            wp_reset_postdata(); ?>
		</div>
    <?php $myvariable = ob_get_clean();
    return $myvariable;
    }
}

add_shortcode( 'agriculture-project', 'agriculture_project' );
function agriculture_project( $atts ) {
    ob_start();
    $query = new WP_Query( array(
        'post_type' => 'agriculture',
        'posts_per_page' => -1,
		'order' => 'ASC' 
    ) );
    if ( $query->have_posts() ) { ?>
        <div class="flex flex-wrap items-center px-4 md:px-40">
				<?php while ( $query->have_posts() ) : $query->the_post(); $counter++;
				$post_id = get_the_ID(); 
				if($counter%2 != 0){
				?>
					<div class="w-full md:w-1/2 p-4">
						<h1 class="text-3xl font-semibold heading-red"><?php the_title(); ?></h1>
						<?php the_content();?>
						
					</div>
					<div class="w-full md:w-1/2 p-4 md:p-16 text-gray-700">
						<img src="<?php the_field('featured_image'); ?>" alt="" width="100%" class="inline-block">
					</div>
			<?php }else { ?>
					<div class="w-full md:w-1/2 p-4 md:p-16 text-gray-700">
						<img src="<?php the_field('featured_image'); ?>" alt="" width="100%" class="inline-block">
					</div>	
					<div class="w-full md:w-1/2 p-4">
						<h1 class="text-3xl font-semibold heading-red"><?php the_title(); ?></h1>
						<?php the_content();?>
						
					</div>
			
			<?php } endwhile;
            wp_reset_postdata(); ?>
		</div>
    <?php $myvariable = ob_get_clean();
    return $myvariable;
    }
}



