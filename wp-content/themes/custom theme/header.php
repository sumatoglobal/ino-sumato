<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
<link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
<script src="https://kit.fontawesome.com/124dfeb8a2.js"></script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="shadow">
<nav class="hidden md:block relative bg-black text-white">
  <div class="flex md:mx-24 mx-8 items-center justify-between">
    <div class="p-4 md:px-6 mx-auto md:mx-0"><a href="/"><img src="<?php the_field('logo', 'options'); ?>" class="h-10 sm:h-10" alt="logo"></a></div>
    <ul class="hidden md:flex">
	
		
	<li class="hoverable bg-red-extra hover:text-white">
		<a href="#" class="relative block py-6 px-4 lg:p-6 text-sm lg:text-base font-bold bg-red-extra hover:text-white">Solutions</a>
		<div class="p-6 mega-menu z-10 mb-16 sm:mb-0 shadow-xl bg-gray-200">
			<div class="container mx-auto w-full flex flex-wrap justify-between mx-2">
			
			<ul class="px-4 w-full sm:w-1/2 lg:w-1/4 border-red-400 border-b sm:border-r lg:border-b-0 pb-6 pt-6 lg:pt-3">
				<div class="flex items-center">
				<svg class="h-8 mb-3 mr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M3 6c0-1.1.9-2 2-2h8l4-4h2v16h-2l-4-4H5a2 2 0 0 1-2-2H1V6h2zm8 9v5H8l-1.67-5H5v-2h8v2h-2z"/>
				</svg>
				<a href="/products" class="font-bold text-xl text-black text-bold mb-2 hover:text-red-400">Product</a>
				</div>
				<p class="text-gray-700 text-sm">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
				<div class="flex items-center py-3">
				<svg class="h-6 pr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
				</svg>
				<a href="#" class="text-black bold hover:text-red-400">Find out more...</a>
				</div>
			</ul>
			<ul class="px-4 w-full sm:w-1/2 lg:w-1/4 border-red-400 border-b sm:border-r-0 lg:border-r lg:border-b-0 pb-6 pt-6 lg:pt-3">
				<div class="flex items-center">
				<svg class="h-8 mb-3 mr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M4.13 12H4a2 2 0 1 0 1.8 1.11L7.86 10a2.03 2.03 0 0 0 .65-.07l1.55 1.55a2 2 0 1 0 3.72-.37L15.87 8H16a2 2 0 1 0-1.8-1.11L12.14 10a2.03 2.03 0 0 0-.65.07L9.93 8.52a2 2 0 1 0-3.72.37L4.13 12zM0 4c0-1.1.9-2 2-2h16a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4z"/>
				</svg>
				<a href="/industries/" class="font-bold text-xl text-black text-bold mb-2 hover:text-red-400">Industry</a>
				</div>
				<p class="text-gray-700 text-sm">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
				<div class="flex items-center py-3">
				<svg class="h-6 pr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
				</svg>
				<a href="/governments/" class="text-black bold hover:text-red-400">Government</a>
				</div>
				<div class="flex items-center py-3">
				<svg class="h-6 pr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
				</svg>
				<a href="/supply-chain/" class="text-black bold hover:text-red-400">Supply Chain</a>
				</div>
				<div class="flex items-center py-3">
				<svg class="h-6 pr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
				</svg>
				<a href="/industrys/" class="text-black bold hover:text-red-400">Industry</a>
				</div>
				<div class="flex items-center py-3">
				<svg class="h-6 pr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
				</svg>
				<a href="/agricultures/" class="text-black bold hover:text-red-400">Agriculture</a>
				</div>
			</ul>
			<ul class="px-4 w-full sm:w-1/2 lg:w-1/4 border-red-400 border-b sm:border-b-0 sm:border-r md:border-b-0 pb-6 pt-6 lg:pt-3">
				<div class="flex items-center">
				<svg class="h-8 mb-3 mr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M2 4v14h14v-6l2-2v10H0V2h10L8 4H2zm10.3-.3l4 4L8 16H4v-4l8.3-8.3zm1.4-1.4L16 0l4 4-2.3 2.3-4-4z"/>
				</svg>
				<a href="/services" class="font-bold text-xl text-black text-bold mb-2 hover:text-red-400">Services</a>
				</div>
				<p class="text-gray-700 text-sm">We offer a complete solution to help grow your business. Whatever your needs, we've got the experience and track record to provide the web application you need. Our solutions include Enterprise level cognitive data analytics, project management, webGIS, mobile apps and system integration.</p>
				<div class="flex items-center py-3">
				<svg class="h-6 pr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
				</svg>
				<a href="#" class="text-black bold hover:text-red-400">Web app / Ecommerce</a>
				</div>
				
				<div class="flex items-center py-3">
				<svg class="h-6 pr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
				</svg>
				<a href="#" class="text-black bold hover:text-red-400">Mobile applications android / ios</a>
				</div>
				
				<div class="flex items-center py-3">
				<svg class="h-6 pr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
				</svg>
				<a href="#" class="text-black bold hover:text-red-400">Enterprise Software</a>
				</div>
				<div class="flex items-center py-3">
				<svg class="h-6 pr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
				</svg>
				<a href="#" class="text-black bold hover:text-red-400">Cloud Computing</a>
				</div>
				
				<div class="flex items-center py-3">
				<svg class="h-6 pr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
				</svg>
				<a href="#" class="text-black bold hover:text-red-400">Graphic Design</a>
				</div>
				
			</ul>
			<ul class="px-4 w-full sm:w-1/2 lg:w-1/4 border-gray-600 pb-6 pt-6 lg:pt-3">
				<div class="flex items-center">
				<svg class="h-8 mb-3 mr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M2 4v14h14v-6l2-2v10H0V2h10L8 4H2zm10.3-.3l4 4L8 16H4v-4l8.3-8.3zm1.4-1.4L16 0l4 4-2.3 2.3-4-4z"/>
				</svg>
				<a href="/training" class="font-bold text-xl text-black text-bold mb-2 hover:text-red-400">Training</a>
				</div>
				<p class="text-gray-700 text-sm">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
				<div class="flex items-center py-3">
				<svg class="h-6 pr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
				</svg>
				<a href="#" class="text-black bold hover:text-red-400">Find out more...</a>
				</div>
			</ul>

			
			</div>
		</div>
		</li>

		  
          <!--Hoverable Link-->
		<li class="hoverable bg-red-extra hover:text-white">
		<a href="/about" class="relative block py-6 px-4 lg:p-6 text-sm lg:text-base font-bold bg-red-extra hover:text-white">About</a>
		<div class="p-6 z-10 mega-menu mb-16 sm:mb-0 shadow-xl bg-gray-200">
			<div class="container mx-auto w-full flex flex-wrap justify-between mx-2">
			
			<ul class="px-4 w-full sm:w-1/2 lg:w-1/4 border-red-400 border-b sm:border-r lg:border-b-0 pb-6 pt-6 lg:pt-3">
				<div class="flex items-center">
				<svg class="h-8 mb-3 mr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M3 6c0-1.1.9-2 2-2h8l4-4h2v16h-2l-4-4H5a2 2 0 0 1-2-2H1V6h2zm8 9v5H8l-1.67-5H5v-2h8v2h-2z"/>
				</svg>
				<a href="/about/#sections1" class="font-bold text-xl text-black text-bold mb-2 hover:text-red-400">Who we are</a>
				</div>
				<p class="text-gray-700 text-sm">Sumato is one of the leading providers of full-service B2B solutions, operating business decision making.</p>
				<div class="flex items-center py-3">
				<svg class="h-6 pr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
				</svg>
				<a href="/about/#sections2" class="text-black bold hover:text-red-400">About Us</a>
				</div>

				<div class="flex items-center py-3">
				<svg class="h-6 pr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
				</svg>
				<a href="/about/#sections3" class="text-black bold hover:text-red-400">Values</a>
				</div>

				<div class="flex items-center py-3">
				<svg class="h-6 pr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
				</svg>
				<a href="/about/#sections4" class="text-black bold hover:text-red-400">How we work</a>
				</div>

				<div class="flex items-center py-3">
				<svg class="h-6 pr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
				</svg>
				<a href="/about/#sections5" class="text-black bold hover:text-red-400">Our team</a>
				</div>
			</ul>
			<ul class="px-4 w-full sm:w-1/2 lg:w-1/4 border-red-400 border-b sm:border-r-0 lg:border-r lg:border-b-0 pb-6 pt-6 lg:pt-3">
				<div class="flex items-center">
				<svg class="h-8 mb-3 mr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M4.13 12H4a2 2 0 1 0 1.8 1.11L7.86 10a2.03 2.03 0 0 0 .65-.07l1.55 1.55a2 2 0 1 0 3.72-.37L15.87 8H16a2 2 0 1 0-1.8-1.11L12.14 10a2.03 2.03 0 0 0-.65.07L9.93 8.52a2 2 0 1 0-3.72.37L4.13 12zM0 4c0-1.1.9-2 2-2h16a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4z"/>
				</svg>
				<a href="/contact/#sections1" class="font-bold text-xl text-black text-bold mb-2 hover:text-red-400">Career</a>
				</div>
				<p class="text-gray-700 text-sm">Every job has certain qualifications and skills that are needed, but we prefer to focus on unique aspects that would make someone an asset to our team</p>
				<div class="flex items-center py-3">
				<svg class="h-6 pr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
				</svg>
				<a href="mailto:careers@sumato.global" class="text-black bold hover:text-red-400">Apply to click here...</a>
				</div>
			</ul>
			<ul class="px-4 w-full sm:w-1/2 lg:w-1/4 border-red-400 border-b sm:border-b-0 sm:border-r md:border-b-0 pb-6 pt-6 lg:pt-3">
				<div class="flex items-center">
				<svg class="h-8 mb-3 mr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M2 4v14h14v-6l2-2v10H0V2h10L8 4H2zm10.3-.3l4 4L8 16H4v-4l8.3-8.3zm1.4-1.4L16 0l4 4-2.3 2.3-4-4z"/>
				</svg>
				<a href="/contact/#sections2" class="font-bold text-xl text-black text-bold mb-2 hover:text-red-400">Brand Identity</a>
				</div>
				<p class="text-gray-700 text-sm">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
				<div class="flex items-center py-3">
				<svg class="h-6 pr-3 fill-current text-red-extra"
					xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
					<path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
				</svg>
				<a href="#" class="text-black bold hover:text-red-400">Find out more...</a>
				</div>
			</ul>
			<ul class="px-4 w-full sm:w-1/2 lg:w-1/4 border-gray-600 pb-6 pt-6 lg:pt-3">
				<div class="flex items-center">
					<svg class="h-8 mb-3 mr-3 fill-current text-red-extra"
						xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
						<path d="M2 4v14h14v-6l2-2v10H0V2h10L8 4H2zm10.3-.3l4 4L8 16H4v-4l8.3-8.3zm1.4-1.4L16 0l4 4-2.3 2.3-4-4z"/>
					</svg>
				<a href="/contact/#sections3" class="font-bold text-xl text-black text-bold mb-2 hover:text-red-400">Find us here</a>
				</div>
                <li class="pt-3">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3580.1200068092758!2d91.75065121545582!3d26.19277418344292!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x375a5989852ca743%3A0x75c4e068f5c1571c!2sSumato%20Globaltech!5e0!3m2!1sen!2sin!4v1567231258837!5m2!1sen!2sin" width="300" height="200" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </li>
            </ul>

			
			</div>
		</div>
		</li>


		

		<!--Regular Link-->
		<li class="bg-red-extra hover:text-white">
			<a href="/developer" class="relative block py-6 px-4 lg:p-6 text-sm lg:text-base font-bold bg-red-extra hover:text-white">Developer</a>
		</li>

		<!--Regular Link-->
		<li class="bg-red-extra hover:text-white">
			<a href="/partner" class="relative block py-6 px-4 lg:p-6 text-sm lg:text-base font-bold bg-red-extra hover:text-white">Partner</a>
		</li>
		  
        
		</ul>
<!-- 		<button class="hidden md:flex bg-transparent bg-outline-red border-outline-red text-red-extra font-semibold hover:text-white py-2 px-4 border-2 hover:border-transparent rounded ml-4">
        Log In
      </button> -->
	  <?php echo do_shortcode('[google-translator]'); ?>
      </div>
    </nav>
</div>






	<?php if(!is_front_page()){
if(get_field('banner_image')!=''){
	$banner = get_field('banner_image');
}else{
	$banner = get_field('default_banner_image', 'options');
}
	if(get_field('banner_height')!=''){
	$bannerH = get_field('banner_height');
}
?>
	<div class="banner bg-gray-800 flex items-center" style="background-size: cover; background-image:url(<?php echo $banner ?>);<?php if(get_field('banner_height')!='') {echo 'min-height:'.$bannerH.'px;';} ?>">
		
	</div>
<?php } ?>
<div id="page" class="hfeed site">
	<div id="main" class="wrapper">
