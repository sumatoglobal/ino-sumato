<?php get_header(); ?>


<div class="flex">
  	<div class="w-full h-auto relative bg-gray-600" style="background-image:url('<?php the_field('header_image', 'options'); ?>'); background-size: cover;">
			<div class="flex p-3 pb-8 md:p-24 md:pb-64 text-white">
				<div class="w-3/5 px-1 sm:px-8 md:px-12 pt-1 sm:pt-8 md:pt-16 mb-4 sm:pb-16 md:pb-32">
					<?php the_field('header_left_content', 'options'); ?>
				</div>
				<div class="w-2/5">
					<?php the_field('header_right_content', 'options'); ?>
				</div>
			</div>
			
		
		
		
		<div class="absolute bottom-0 w-full">
			<img src="/wp-content/uploads/2019/09/bg-white-swoosh.svg" class="w-full">
		</div>
			
	</div>	
</div>

<div class="relative md:h-8">
  <div class="flex flex-wrap md:absolute md:bottom-0 w-full px-4 md:px-24">
  	<div class="w-full sm:w-1/2 md:w-1/4">
		<div class="relative rounded-lg text-center bg-cta-transparent text-gray-700 shadow-lg m-5">
			<div class="absolute w-full cta-icon">
				<img class="bg-gray-200 p-2 rounded-full shadow-lg inline-block w-16 sm:w-20 md:w-24 border-8 border-teal-600" src="<?php the_field('homepage_cta_icon_1', 'options'); ?>" alt="">
			</div>
			<div class="px-8 pt-20 pb-8">
				<div class="font-bold text-xl mb-2"><?php the_field('homepage_cta_header_1', 'options'); ?></div>
				<p class="text-gray-700 text-base"><?php the_field('homepage_cta_content_1', 'options'); ?></p>
			</div>
		</div>
	  </div>
	  <div class="w-full sm:w-1/2 md:w-1/4">
		<div class="relative rounded-lg text-center bg-cta-transparent text-gray-700 shadow-lg m-5">
			<div class="absolute w-full cta-icon">
			  <img class="bg-gray-200 p-2 rounded-full shadow-lg inline-block w-16 sm:w-20 md:w-24 border-8 border-teal-600" src="<?php the_field('homepage_cta_icon_2', 'options'); ?>" alt="">
			</div>
			<div class="px-8 pt-20 pb-8">
				<div class="font-bold text-xl mb-2"><?php the_field('homepage_cta_header_2', 'options'); ?></div>
				<p class="text-gray-700 text-base"><?php the_field('homepage_cta_content_2', 'options'); ?></p>
			</div>
		</div>
	  </div>
	  <div class="w-full sm:w-1/2 md:w-1/4">
		<div class="relative rounded-lg text-center bg-cta-transparent text-gray-700 shadow-lg m-5">
			<div class="absolute w-full cta-icon">
			  <img class="bg-gray-200 p-2 rounded-full shadow-lg inline-block w-16 sm:w-20 md:w-24 border-8 border-teal-600" src="<?php the_field('homepage_cta_icon_3', 'options'); ?>" alt="">
			</div>
			<div class="px-8 pt-20 pb-8">
				<div class="font-bold text-xl mb-2"><?php the_field('homepage_cta_header_3', 'options'); ?></div>
				<p class="text-gray-700 text-base"><?php the_field('homepage_cta_content_3', 'options'); ?></p>
			</div>
		</div>
	  </div>
	  <div class="w-full sm:w-1/2 md:w-1/4">
		<div class="relative rounded-lg text-center bg-cta-transparent text-gray-700 shadow-lg m-5">
			<div class="absolute w-full cta-icon">
			  <img class="bg-gray-200 p-2 rounded-full shadow-lg inline-block w-16 sm:w-20 md:w-24 border-8 border-teal-600" src="<?php the_field('homepage_cta_icon_4', 'options'); ?>" alt="">
			</div>
			<div class="px-8 pt-20 pb-8">
				<div class="font-bold text-xl mb-2"><?php the_field('homepage_cta_header_4', 'options'); ?></div>
				<p class="text-gray-700 text-base"><?php the_field('homepage_cta_content_4', 'options'); ?></p>
			</div>
		</div>
	  </div>
  </div>
</div>

<div class="flex flex-wrap mx-4 md:mx-24">
		<div class="sm:hidden mx-auto w-2/3 sm:w-1/2 p-2 md:p-24 text-center">
			<img class="inline-block" src="<?php the_field('section_1_icon', 'options'); ?>" height="300" width="300" alt="">
		</div>
		<div class="w-full sm:w-1/2 p-4 md:p-24">
			<?php the_field('section_1_content', 'options'); ?>
		</div>
		<div class="hidden sm:block w-1/2 p-2 md:p-24 text-center">
			<img class="inline-block" src="<?php the_field('section_1_icon', 'options'); ?>" height="300" width="300" alt="">
		</div>
</div>


<div class="flex flex-wrap mx-4 md:mx-24">
		<div class="mx-auto w-2/3 sm:w-1/2 p-2 md:p-24 text-center">
			<img class="inline-block" src="<?php the_field('section_2_icon', 'options'); ?>" height="400" width="400" alt="">
		</div>
		<div class="w-full sm:w-1/2 p-4 md:p-24">
			<?php the_field('section_2_content', 'options'); ?>
		</div>
</div>

<div class="flex flex-wrap mx-4 md:mx-24">
		<div class="sm:hidden mx-auto w-2/3 sm:w-1/2 p-2 md:p-24 text-center">
			<img class="inline-block" src="<?php the_field('section_3_icon', 'options'); ?>" height="400" width="400" alt="">
		</div>
		<div class="w-full sm:w-1/2 p-4 md:p-24">
			<?php the_field('section_3_content', 'options'); ?>
		</div>
		<div class="hidden sm:block w-1/2 p-2 md:p-24 text-center">
			<img class="inline-block" src="<?php the_field('section_3_icon', 'options'); ?>" height="400" width="400" alt="">
		</div>
</div>


<div class="flex text-center background-parallax">
	<div class="w-4/5 md:w-3/5 my-10 p-8 md:my-24 mx-auto md:p-16 bg-cta-transparent-1 rounded-lg">
		<?php the_field('parallax_content', 'options'); ?>
	</div>
</div>




<?php
if( have_rows('sections', 'options') ):
$count=1;
    while ( have_rows('sections', 'options') ) : the_row();
?>
<div id="sections<?php echo $count ?>" class="sections entry-content <?php the_sub_field('custom_classes') ?>" style="background-color:<?php the_sub_field('background_color') ?>;background-image:url(<?php the_sub_field('background_image') ?>);color:<?php the_sub_field('color') ?>;padding:<?php the_sub_field('padding') ?> 0px">
		<?php the_sub_field('content') ?>
</div>
<?php
$count++;
    endwhile;
endif;
?>
<?php get_footer(); 