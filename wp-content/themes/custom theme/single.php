<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 */

get_header(); ?>




<?php while ( have_posts() ) : the_post(); ?>
<div class="flex flex-wrap items-center px-24">
	<div class="w-full md:w-1/2 p-16 text-gray-700">
		<img src="<?php the_field('featured_image'); ?>" alt="" width="100%" class="inline-block">
	</div>

	<div class="w-full md:w-1/2 p-4">
		<h1 class="text-3xl font-semibold heading-red"><?php the_title(); ?></h1>
		<?php the_content();?>
	</div>
	
	<nav class="nav-single">
		<span class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '←', 'Previous post link', 'custom' ) . '</span> %title' ); ?></span>
		<span class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '→', 'Next post link', 'custom' ) . '</span>' ); ?></span>
	</nav>
</div>
	
	



<?php endwhile; ?>








<?php get_footer();